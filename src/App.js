import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import './App.css';
import {AppBar, Button, Container, IconButton, Toolbar} from "@mui/material";
import MenuIcon from '@mui/icons-material/Menu';
import {BrowserRouter, Link, Route, Routes} from "react-router-dom";

import {firebaseInit, logout} from "./app/firebase";
import {getAuth, onAuthStateChanged} from "firebase/auth";

import {Counter} from './features/counter/Counter';
import {getAuthUser, loggedIn, loggedOut} from "./features/authentication/authenticationSlice";
import {Authentication} from "./features/authentication/Authentication";
import {PostsList} from "./features/posts/PostsList";
import {PostsAdd} from "./features/posts/PostsAdd";

firebaseInit();

function App() {
    const dispatch = useDispatch();
    const user = useSelector(getAuthUser);
    const loading = useSelector((state) => state.authentication.loading);

    // hook used in functional components
    // https://reactjs.org/docs/hooks-effect.html
    useEffect(() => {
        // called in componentDidMount
        const auth = getAuth();
        onAuthStateChanged(auth, (user) => {
            if (user) {
                // User is signed in, see docs for a list of available properties
                // https://firebase.google.com/docs/reference/js/firebase.User
                dispatch(loggedIn({
                    displayName: user.displayName,
                    email: user.email
                }));
            } else {
                // User is signed out
                // ...
                dispatch(loggedOut());
            }
        });
        return () => {
            // called in componentWillUnMount
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []); // don't forget the brackets, otherwise the function will be called many times

    let content;
    if (loading) {
        content = <h1>Loading...</h1>;
    } else {
        if (user) {
            content = <LoggedIn/>;
        } else {
            content = <LoggedOut/>;
        }
    }

    return (
        content
    );
}

function LoggedIn() {
    console.log("LoggedIn");
    const user = useSelector(getAuthUser);

    return (
        <BrowserRouter>
            <React.Fragment>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton edge="start" color="inherit" aria-label="menu">
                            <MenuIcon/>
                        </IconButton>
                        <Button color="inherit" component={Link} to="/">Home</Button>
                        <Button color="inherit" component={Link} to="/about">About</Button>
                        <Button color="inherit" component={Link} to="/about">About UNIPI</Button>
                        <Button color="inherit" component={Link} to="/counter">Counter</Button>
                        <Button color="inherit" component={Link} to="/posts">Posts</Button>
                        <Button color="inherit" onClick={() => logout()}>Logout</Button>
                    </Toolbar>
                </AppBar>
                <div className="App">
                    <header className="App-header">
                        <Routes>
                            <Route path="/" element={
                                <Container>
                                    Hello {user.displayName}
                                </Container>
                            }/>
                            <Route path="/about" element={
                                <div>About</div>
                            }/>
                            <Route path="/counter" element={
                                <Counter/>
                            }/>
                            <Route path="/posts" element={
                                <PostsList/>
                            }/>
                            <Route path="/posts/add" element={
                                <PostsAdd/>
                            }/>
                        </Routes>
                    </header>
                </div>
            </React.Fragment>
        </BrowserRouter>
    );
}

function LoggedOut() {
    console.log("LoggedOut");
    return (
        <Authentication/>
    );
}

export default App;
