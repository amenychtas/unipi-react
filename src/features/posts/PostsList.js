import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import Button from "@mui/material/Button";
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Container from '@mui/material/Container';
import {Link} from "react-router-dom";
import {postsSubscribe, postsUnsubscribe} from "./postsSlice";

function Post({post}) {
    return (
        <div className="m-1">
            <Card>
                <CardContent>
                    <h2>{post.title}</h2>
                    <div>{post.content}</div>
                </CardContent>
            </Card>
        </div>
    );
}

export function PostsList() {
    const posts = useSelector((state) => state.posts.posts);
    const dispatch = useDispatch();

    useEffect(() => {
        // onComponentDidMount
        dispatch(postsSubscribe());

        return () => {
            // onComponentWillUnmount
            dispatch(postsUnsubscribe());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Container>
            {
                posts.map((post) => (
                    <Post key={post.id} post={post}/>
                ))
            }
            <Button variant="contained" color="secondary" component={Link} to="/posts/add">
                Add
            </Button>
        </Container>
    );
}