import React, {useState} from 'react';
import Button from "@mui/material/Button";
import TextField from '@mui/material/TextField';
import {addPost} from './postsSlice'
import {useNavigate} from "react-router-dom";

export function PostsAdd() {
    const [title, setTitle] = useState('')
    const [content, setContent] = useState('')
    const navigate = useNavigate()

    const onTitleChanged = (e) => setTitle(e.target.value)
    const onContentChanged = (e) => setContent(e.target.value)

    return (
        <form noValidate autoComplete="off">
            <TextField id="post-title"
                       label="Title"
                       value={title}
                       onChange={onTitleChanged}/>
            <br/>
            <TextField id="post-content"
                       label="Content"
                       value={content}
                       onChange={onContentChanged}/>
            <br/>
            <Button color="secondary"
                    onClick={() => {
                        addPost({
                            title,
                            content
                        });
                        navigate("/posts");
                    }}>
                Save
            </Button>
        </form>
    );
}
