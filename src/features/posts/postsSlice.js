import {createSlice} from '@reduxjs/toolkit';

import {addDoc, collection, getFirestore, onSnapshot, query} from "firebase/firestore";

export const postsSlice = createSlice({
    name: 'posts',
    initialState: {
        posts: [],
    },
    reducers: {
        postsUpdated: (state, action) => {
            state.posts = action.payload
        }
    },
});

let unsubscribe;
export const postsSubscribe = () => (dispatch) => {
    const db = getFirestore();
    const postCollection = collection(db, "posts");
    const q = query(postCollection);

    unsubscribe = onSnapshot(q, (querySnapshot) => {
        let posts = [];
        querySnapshot.forEach(function (doc) {
            const data = doc.data();
            data.id = doc.id;
            posts.push(data);
            // console.log(data);
        });
        dispatch(postsUpdated(posts));
    });
}

export const postsUnsubscribe = () => (dispatch) => {
    unsubscribe();
}

// do we need dispatch here? what do you think?
export const addPost = post => {
    const db = getFirestore();
    const postCollection = collection(db, "posts");

    addDoc(postCollection, post)
        .then(function (docRef) {
            console.log(docRef.id);
        })
        .catch(function (error) {
            console.error("Error adding document: ", error);
        });
};

export const {postsUpdated} = postsSlice.actions;

export default postsSlice.reducer;