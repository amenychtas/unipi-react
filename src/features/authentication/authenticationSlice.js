import {createSlice} from '@reduxjs/toolkit';

export const authenticationSlice = createSlice({
    name: 'authentication',
    initialState: {
        user: null,
        loading: true,
    },
    reducers: {
        loggedIn: (state, action) => {
            state.user = action.payload;
            state.loading = false;
        },
        loggedOut: state => {
            state.user = null;
            state.loading = false;
        }
    },
});

export const {loggedIn, loggedOut} = authenticationSlice.actions;

export const getAuthUser = state => state.authentication.user;

export default authenticationSlice.reducer;