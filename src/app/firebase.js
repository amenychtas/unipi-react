import {initializeApp} from "firebase/app";
import {getAuth, signOut} from "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyBYSg0KQPO9nr1CO8hIBzGxTeXfQLR1VmU",
    authDomain: "unipi-aps.firebaseapp.com",
    databaseURL: "https://unipi-aps-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "unipi-aps",
    storageBucket: "unipi-aps.appspot.com",
    messagingSenderId: "970777890964",
    appId: "1:970777890964:web:b3bf8f499707a72daaa481"
};

export const firebaseInit = function () {
    initializeApp(firebaseConfig);
}

export const logout = async function () {
    // we could use promises and then/catch. More info here:
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises
    const auth = getAuth();
    await signOut(auth);
}