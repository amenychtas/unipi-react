import React, {useEffect} from 'react';
import {getAuth, EmailAuthProvider, GoogleAuthProvider} from "firebase/auth";

import * as firebaseui from 'firebaseui'

import 'firebaseui/dist/firebaseui.css'

export function Authentication() {
    const auth = getAuth();

    // hook used in functional components
    // https://reactjs.org/docs/hooks-effect.html
    useEffect(() => {
        // called in componentDidMount
        let ui = new firebaseui.auth.AuthUI(auth);
        ui.start('#firebaseui-auth-container', {
            signInOptions: [
                EmailAuthProvider.PROVIDER_ID,
                GoogleAuthProvider.PROVIDER_ID
            ],
            // Other config options...
            // https://firebase.google.com/docs/auth/web/firebaseui
        });

        return () => {
            // called in componentWillUnMount
            ui.delete().then().catch();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []); // don't forget the brackets, otherwise the function will be called many times

    return (
        <div id='firebaseui-auth-container'/>
    );
}