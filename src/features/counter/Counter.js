import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {decrement, increment, incrementAsync, incrementByAmount, incrementIfOdd, selectCount,} from './counterSlice';
import styles from './Counter.module.css';

import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import {IconButton} from '@mui/material';

export function Counter() {
    const count = useSelector(selectCount);
    const dispatch = useDispatch();
    const [incrementAmount, setIncrementAmount] = useState('2');

    const incrementValue = Number(incrementAmount) || 0;

    return (
        <div>
            <div className={styles.row}>
                <IconButton aria-label="Remove"
                            onClick={() => dispatch(decrement())}>
                    <RemoveCircleIcon/>
                </IconButton>
                <span className={styles.value}>{count}</span>
                <IconButton aria-label="Add"
                            onClick={() => dispatch(increment())}>
                    <AddCircleIcon/>
                </IconButton>
            </div>
            <div className={styles.row}>
                <input
                    className={styles.textbox}
                    aria-label="Set increment amount"
                    value={incrementAmount}
                    onChange={(e) => setIncrementAmount(e.target.value)}
                />
                <button
                    className={styles.button}
                    onClick={() => dispatch(incrementByAmount(incrementValue))}
                >
                    Add Amount
                </button>
                <button
                    className={styles.asyncButton}
                    onClick={() => dispatch(incrementAsync(incrementValue))}
                >
                    Add Async
                </button>
                <button
                    className={styles.button}
                    onClick={() => dispatch(incrementIfOdd(incrementValue))}
                >
                    Add If Odd
                </button>
            </div>
        </div>
    );
}
